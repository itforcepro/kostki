﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kostki
{
    class Bucket
    {
        int IleKostek;
        int IleScian;
        Random rand = new Random(Guid.NewGuid().GetHashCode());

        public Bucket(int ileKostek, int ileScian)
        {
            this.IleKostek = ileKostek;
            this.IleScian = ileScian;
        }

        protected int rzut()
        {
            return this.rand.Next(0, this.IleScian);
        }

        public List<int> rzuc()
        {
            var rzuty = new List<int>(IleKostek);

            for (int i = 0; i < IleKostek; i++)
            {
                rzuty.Add(this.rzut());
            }
            return rzuty;
        }
    }
}

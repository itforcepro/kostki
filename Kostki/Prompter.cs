using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kostki
{
    class Prompter
    {
        public int getInt(string message = "Niepoprawna wartosc, sprob�j ponownie")
        {
            int x = 0;
            while (!int.TryParse(Console.ReadLine(), out x))
            {
                Console.WriteLine(message);
            }
            return x;
        }
        public int promptForInt(string prompt)
        {
            Console.WriteLine(prompt);
            return getInt();
        }
    }
}